import React, { Component } from 'react';
import { Router, Route, Link, browserHistory } from 'react-router';
import { Alert,Button, Card, CardBody, CardGroup, Col, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import './App.css';
import Modal from 'react-responsive-modal';
import ReCAPTCHA from "react-google-recaptcha";
import AuthService from './components/AuthService';
import swal from 'sweetalert';
import Select from 'react-select';
import BaseApi from './components/BaseApi';
import createHistory from 'history/createBrowserHistory'
 
const history = createHistory()
const Auth = new AuthService();
const recaptchaRef = React.createRef();

class App2 extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
        username: '',
        password: '',
        open: false,
        studentLoginOpen: false,
        studentOpen: false,
        submitted: false,
        captch:false,
        restext:'Sign In to your account',
        college_details: null,
        course_details:null,
        college:[],
        course:[], 
        resstudenttext:'',
        visible: false,
        alerttext:'',
        loggedIn:false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.ReCAPTCHAGGet = this.ReCAPTCHAGGet.bind(this);
    this.onChange = this.onChange.bind(this); 
    this.onDismiss = this.onDismiss.bind(this);
    this.LogOut = this.LogOut.bind(this);
    
}

onDismiss() {
  this.setState({ visible: false });
}



onChange(e){
  this.setState({[e.target.name]:e.target.value});
}

componentWillMount() {

console.log('====================================');
console.log(history);
console.log('====================================');
  var searchParam = history.location.search;
  if (searchParam) {
    if(searchParam === '?status=logout'){
      Auth.logout(); // Setting the token in localStorage
      this.setState({loggedIn:false});
    }
  }else if(Auth.loggedIn()){
  this.setState({loggedIn:true});
  }

 

}






  onOpenModal = () => {
    this.setState({ open: true });
  };

  onStudentLoginOpenModal = () => {
    this.setState({ studentLoginOpen: true });
  };

  onStudentOpenModal = () => {
    this.setState({ studentOpen: true });
    this.setState({ alerttext: "" , visible : false});
  };

  onCloseModal = () => {
    this.setState({ open: false });
    this.setState({ studentOpen: false });
    this.setState({ studentLoginOpen: false });
  };

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
}
  
handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true,restext:'Loading....' });
    const { username, password ,captch} = this.state;  
    if (username && password) {
     
      
        Auth.login(username,password,'users/login')
        .then(res =>{
          console.log(res);
          if (res.status) {
          window.location.replace(BaseApi.login_url+'/#/login?key='+res.data.loginToken);
          }else{
          this.setState({restext:"Incorrect Email or Password"});
          }
          // this.props.history.replace('http://localhost:3001/#/login?key='+res.data.loginToken);
          //this.setState({restext:'mail and password required'});

        })
        .catch(err =>{
          console.log(err);
          this.setState({restext:"Something's wrong. Please check your connection and try again"});
        })
    
        
     

    }else{
      this.setState({restext:'Email and Password are required'});
    }
};

ReCAPTCHAGGet(value) {
  const recaptchaValue = recaptchaRef.current.getValue();
  // this.props.onSubmit(recaptchaValue);
  console.log("Captcha value:", recaptchaValue);
  this.setState({captch:true});
}


LogOut(){
  Auth.logout();
  this.setState({loggedIn:false});
    
}



  render() {
    const { username, password,college_details,course_details,college,course,alerttext } = this.state;
    const { open ,studentOpen, studentLoginOpen} = this.state;
    
    return (
      <div >
      <footer style={{paddingTop: '0px', marginTop: '-20px'}}>
        <div className="subfooter">
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                {this.state.loggedIn ? (
                  <p>2019 © Copyright <a href="#">CAS.</a> </p>
                ):(
                  <p>2019 © Copyright <a href="#">CAS.</a><a className="admin_zone"  onClick={this.onOpenModal}>Admin Zone</a></p>
                )}
              </div>
              <div className="col-md-6" style={{textAlign: 'right'}}>
                Developed by <a href="http://filoi.in" target="_blank">Filoi</a>
              </div>
            </div>
          </div>
        </div>
      </footer>

     
         <Modal open={open} onClose={this.onCloseModal} closeOnOverlayClick={false} center>
         <CardGroup>
                <Card className="p-6">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <h2>Admin Login</h2>
                      <p className="text-muted" >{this.state.restext}</p>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            Email
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" className="narrow-input" placeholder="Email" autoComplete="username" name="username" value={username} onChange={this.handleChange} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            Password
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" className="narrow-input" placeholder="Password" autoComplete="current-password" name="password" value={password} onChange={this.handleChange} />
                      </InputGroup>
                      <InputGroup className="mb-4" style={{textAlign: "center", marginBottom: "20px"}}>
                      <ReCAPTCHA
                          ref={recaptchaRef}
                          sitekey={BaseApi.captcha_key}
                          onChange={this.ReCAPTCHAGGet}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                      <Col xs="6">
                          <Button color="primary" className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          Forgot password?
                        </Col>
                      </InputGroup>
                    
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
        </Modal>
      </div>
    );
  }
}

export default App2;